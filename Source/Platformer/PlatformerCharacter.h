// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "Engine/EngineTypes.h"
#include "TimerManager.h"
#include "P_WeaponBase.h"
#include "P_FunctionLibrary.h"
#include "P_InterfaceHelper.h"
#include "PlatformerCharacter.generated.h"

class ATargetPoint;
class UBoxComponent;
class UP_HealthComponent;
class UP_InventoryComponent;
class UPointLightComponent;

UCLASS(config=Game)
class APlatformerCharacter : public ACharacter, public IP_InterfaceHelper
{
	GENERATED_BODY()

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* SideViewCameraComponent;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UPointLightComponent* Light;


public:
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UBoxComponent* DropItemHolder;

	// AI
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TArray<ATargetPoint*> PatrolPoints; 

	// Light
	FORCEINLINE UPointLightComponent* GetLight() { return Light; }

	UFUNCTION(NetMulticast, Reliable, BlueprintCallable)
	void ActivateFlashlight();
	UFUNCTION(NetMulticast, Reliable, BlueprintCallable)
	void DeactivateFlashlight();

	void ShiftPressed();
	void ShiftReleased();
	
	UPROPERTY(BlueprintReadWrite)
	bool bShiftPressed = false;

	UFUNCTION(BlueprintCallable)
	void ToggleFlashlightClient();
	UFUNCTION(Server, Reliable, BlueprintCallable)
	void ToggleFlashlight();
	UPROPERTY();
	bool FlashLightActive = true;

	// Health
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Character Settings", meta = (AllowPrivateAccess = "true"))
	UP_HealthComponent* HealthComponent = nullptr;
	UFUNCTION(BlueprintCallable)
	void SetHealthComponent(UP_HealthComponent* NewHealthComponent) { HealthComponent = NewHealthComponent; };
	UFUNCTION(BlueprintCallable)
	UP_HealthComponent* GetHealthComponent() { return HealthComponent; };


	// Inventory
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Character Settings", meta = (AllowPrivateAccess = "true"))
	UP_InventoryComponent* InventoryComponent = nullptr;

	UFUNCTION(BlueprintCallable)
	void SetInventoryComponent(UP_InventoryComponent* NewInventoryComponent);
	UFUNCTION(BlueprintCallable)
	UP_InventoryComponent* GetInventoryComponent(bool& bHasInventory);

	// Weapon things
	UFUNCTION(BlueprintCallable)
	void SetCurrentWeapon(AP_WeaponBase* NewWeapon);
	UFUNCTION(BlueprintCallable)
	AP_WeaponBase* GetCurrentWeapon(bool& bHasWeapon);
	UPROPERTY(Replicated, VisibleAnywhere, BlueprintReadOnly, Category = "Character Settings")
	AP_WeaponBase* CurrentWeapon = nullptr;
	UFUNCTION(BlueprintCallable)
	void TryFireCurrentWeapon(UAnimMontage* FireAnim, FRotator FireDirection, const TArray<TEnumAsByte<EObjectTypeQuery>>& Query, float Damage);
	UFUNCTION(BlueprintCallable)
	void SwitchCooldownIsFinished(bool bIsFinished);

	FTimerDelegate WeaponCDDelegate;
	FTimerHandle WeaponCoolDownTimer;
	bool bWeaponCooldownFinished;
	TSubclassOf<AP_WeaponBase> LastCooldownedWeapon;


	// Fire Direction
	UPROPERTY(VisibleDefaultsOnly, BlueprintReadOnly, Category = "Character Settings", meta = (AllowPrivateAccess = "true"))
	ELookTo LastLookDirUp = ELookTo::Up;
	UPROPERTY(VisibleDefaultsOnly, BlueprintReadOnly, Category = "Character Settings", meta = (AllowPrivateAccess = "true"))
	ELookTo LastLookDirRight = ELookTo::Right;
	UPROPERTY(VisibleDefaultsOnly, BlueprintReadOnly, Category = "Character Settings", meta = (AllowPrivateAccess = "true"))
	ELookTo LookDirActual = ELookTo::Right;
	UPROPERTY(VisibleDefaultsOnly, BlueprintReadOnly, Category = "Character Settings", meta = (AllowPrivateAccess = "true"))
	FRotator LookRotation;

	bool bUpPressed = false;


	FTimerDelegate TimerDelegate;
	FTimerHandle FirePreDelay;

	void Jump() override;
	void StopJumping() override;

	void PreviousWeapon();
	void NextWeapon();

	void MoveRight(float Val);
	void LookUp(float Val);

protected:


	void TouchStarted(const ETouchIndex::Type FingerIndex, const FVector Location);
	void TouchStopped(const ETouchIndex::Type FingerIndex, const FVector Location);
	virtual void SetupPlayerInputComponent(class UInputComponent* InputComponent) override;


	// Weapon/Attack things
	UFUNCTION(Server, Reliable, BlueprintCallable)
	void FireReq(UAnimMontage* FireAnim, FRotator FireDirection, const TArray<TEnumAsByte<EObjectTypeQuery>>& Query, float Damage);

	UFUNCTION(NetMulticast, Reliable, BlueprintCallable)
	void Fire(UAnimMontage* FireAnim, FRotator FireDirection, const TArray<TEnumAsByte<EObjectTypeQuery>>& Query, float Damage);

	UFUNCTION(Server, Reliable, BlueprintCallable)
	void SpawnWeaponInHands(TSubclassOf<AP_WeaponBase> WeaponClass);

	void Tick(float DeltaSeconds) override;
	void HandleRotation();

	

public:
	APlatformerCharacter(const FObjectInitializer& ObjectInitializer);

	FORCEINLINE class UCameraComponent* GetSideViewCameraComponent() const { return SideViewCameraComponent; }
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }
	void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

	//Interfaces
	virtual ETargetType GetTargetType() override { return ETargetType::Character; };
	virtual void AddDamage(float DamageVal);
};
