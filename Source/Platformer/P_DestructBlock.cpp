// Fill out your copyright notice in the Description page of Project Settings.


#include "P_DestructBlock.h"
#include "Kismet/KismetMathLibrary.h"
#include "Kismet/GameplayStatics.h"
#include "P_PickableItem.h"
#include "Components/StaticMeshComponent.h"

// Sets default values
AP_DestructBlock::AP_DestructBlock()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	BlockMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("BlockMesh"));
	RootComponent = BlockMesh;
	BlockMesh->SetRelativeScale3D(FVector(1, 2, 2));
	BlockMesh->SetCollisionProfileName(TEXT("Ore"));

	SpawnItemHolder = CreateDefaultSubobject<UBoxComponent>(TEXT("ItemHolder"));
	SpawnItemHolder->SetupAttachment(RootComponent);
	SpawnItemHolder->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	SpawnItemHolder->InitBoxExtent(FVector(1.f, 1.f, 1.f));
}

void AP_DestructBlock::OnRep_CurrentHealth()
{
	float DmgConverted = 1 - UKismetMathLibrary::MapRangeClamped(CurrentHealth, 0.f, MaxHealth, 0.f, 1.f);

	if (DMI)
		DMI->SetScalarParameterValue(MaterialDamageParameterName, DmgConverted);

	if (CurrentHealth <= 0)
	{
		OnPreKilled();
		OnKilled();//Destroy();
	}
}

void AP_DestructBlock::SetCurrentHealth(float NewVal)
{
	CurrentHealth = NewVal;
}

void AP_DestructBlock::AddDamage_Implementation(float DamageVal)
{
	if (CurrentHealth - DamageVal <= 0)
		UGameplayStatics::PlaySoundAtLocation(GetWorld(), DestroySound, GetActorLocation());
	else
		UGameplayStatics::PlaySoundAtLocation(GetWorld(), HitSound, GetActorLocation());
	

	if (HasAuthority())
	{
		float NewHealth = CurrentHealth - DamageVal;
		SetCurrentHealth(NewHealth);
		OnRep_CurrentHealth();
	}
}

void AP_DestructBlock::OnPreKilled_Implementation()
{
	if (LootItem)
	{
		FVector SpLoc = SpawnItemHolder->GetComponentLocation();
		FRotator SpRot = SpawnItemHolder->GetComponentRotation();

		FActorSpawnParameters SpParams;
		SpParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;


		GetWorld()->SpawnActor(LootItem, &SpLoc, &SpRot, SpParams);
	}
}

// Called when the game starts or when spawned
void AP_DestructBlock::BeginPlay()
{
	Super::BeginPlay();

	if (Material)
	{
		DMI = BlockMesh->CreateDynamicMaterialInstance(0, Material);
	}

	if (HasAuthority())
	{
		SetReplicates(true);
	}
}

// Construction script
void AP_DestructBlock::OnConstruction(const FTransform& Transform)
{
	Super::OnConstruction(Transform);

	if (Material && BlockMesh)
	{
		BlockMesh->SetMaterial(0, Material);
	}

	CurrentHealth = MaxHealth;
}

void AP_DestructBlock::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(AP_DestructBlock, CurrentHealth);
}

// Called every frame
void AP_DestructBlock::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

