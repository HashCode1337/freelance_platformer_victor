// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "PlatformerGameMode.generated.h"

UCLASS(minimalapi)
class APlatformerGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	APlatformerGameMode();

	UFUNCTION(Server, Reliable, BlueprintCallable)
	void EasySpawnAtLocation(TSubclassOf<AActor> ActorClass, FVector Location);
};



