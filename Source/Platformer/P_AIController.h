// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "P_AIController.generated.h"

/**
 * 
 */
UCLASS()
class PLATFORMER_API AP_AIController : public AAIController
{
	GENERATED_BODY()

private:
	UPROPERTY(EditDefaultsOnly)
	UBehaviorTree* BT;

	UPROPERTY(EditAnywhere)
	FVector FirstLoc;
	UPROPERTY(EditAnywhere)
	FVector SecondLoc;


protected:

	void BeginPlay() override;
	
};
