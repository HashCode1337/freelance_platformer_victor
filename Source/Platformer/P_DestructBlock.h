// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Net/UnrealNetwork.h"

#include "P_InterfaceHelper.h"
#include "P_FunctionLibrary.h"
#include "P_DestructBlock.generated.h"

class UBoxComponent;
class UStaticMeshComponent;
class AP_PickableItem;

UCLASS(Blueprintable)
class PLATFORMER_API AP_DestructBlock : public AActor, public IP_InterfaceHelper
{
	GENERATED_BODY()
	
public:	
	// Constructor
	AP_DestructBlock();


	// Variables
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "BlockSettings", meta = (AllowPrivateAccess = "true"))
	UStaticMeshComponent* BlockMesh = nullptr;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "BlockSettings", meta = (AllowPrivateAccess = "true"))
	UBoxComponent* SpawnItemHolder = nullptr;


	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "BlockSettings")
	float MaxHealth = 100.f;

	UPROPERTY(ReplicatedUsing=OnRep_CurrentHealth/*, EditDefaultsOnly, BlueprintReadWrite, Category = "BlockSettings"*/)
	float CurrentHealth = MaxHealth;
	UFUNCTION()
	void OnRep_CurrentHealth();
	UFUNCTION()
	void SetCurrentHealth(float NewVal);

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "BlockSettings")
	UMaterialInstance* Material = nullptr;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "BlockSettings")
	FName MaterialDamageParameterName = TEXT("Damage");

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "BlockSettings")
	USoundBase* HitSound = nullptr;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "BlockSettings")
	USoundBase* DestroySound = nullptr;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "BlockSettings")
	USoundBase* StepSound = nullptr;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "BlockSettings")
	TSubclassOf<AP_PickableItem> LootItem;

public:

	// Funcs


	UMaterialInstanceDynamic* DMI = nullptr;

private:

	// Vars

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	virtual void OnConstruction(const FTransform& Transform);
	void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Interface
	virtual ETargetType GetTargetType() override { return ETargetType::Block; };

	UFUNCTION(NetMulticast, Reliable, BlueprintCallable)
	void AddDamage(float DamageVal);

	UFUNCTION(Server, Reliable, BlueprintCallable)
	void OnPreKilled();

	UFUNCTION(BlueprintImplementableEvent)
	void OnKilled();

};
