// Copyright Epic Games, Inc. All Rights Reserved.

#include "PlatformerCharacter.h"
#include "Camera/CameraComponent.h"
#include "Components/BoxComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/InputComponent.h"
#include "Components/PointLightComponent.h"
#include "Kismet/KismetSystemLibrary.h"
#include "Kismet/GameplayStatics.h"
#include "Net/UnrealNetwork.h"
#include "GameFramework/SpringArmComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "DrawDebugHelpers.h"

#include "P_CMComponent.h"
#include "P_InventoryComponent.h"
#include "P_DestructBlock.h"
#include "P_HealthComponent.h"

APlatformerCharacter::APlatformerCharacter(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer.SetDefaultSubobjectClass<UP_CMComponent>(ACharacter::CharacterMovementComponentName))
{
	// Set size for collision capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// Don't rotate when the controller rotates.
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Create a camera boom attached to the root (capsule)
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->SetUsingAbsoluteRotation(true); // Rotation of the character should not affect rotation of boom
	CameraBoom->bDoCollisionTest = false;
	CameraBoom->TargetArmLength = 500.f;
	CameraBoom->SocketOffset = FVector(0.f, 0.f, 75.f);
	CameraBoom->SetRelativeRotation(FRotator(0.f, 180.f, 0.f));

	Light = CreateDefaultSubobject<UPointLightComponent>(TEXT("Light"));
	Light->SetupAttachment(CameraBoom);

	DropItemHolder = CreateDefaultSubobject<UBoxComponent>(TEXT("ItemDropPlace"));
	DropItemHolder->SetupAttachment(RootComponent);

	// Create a camera and attach to boom
	SideViewCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("SideViewCamera"));
	SideViewCameraComponent->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
	SideViewCameraComponent->bUsePawnControlRotation = false; // We don't want the controller rotating the camera

	/*HealthComponent = CreateDefaultSubobject<UP_HealthComponent>(TEXT("HealthComponent"));
	HealthComponent->SetIsReplicated(true);
	HealthComponent->allow*/

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Face in the direction we are moving..
	GetCharacterMovement()->RotationRate = FRotator(0.0f, 720.0f, 0.0f); // ...at this rotation rate
	GetCharacterMovement()->GravityScale = 2.f;
	GetCharacterMovement()->AirControl = 0.80f;
	GetCharacterMovement()->JumpZVelocity = 1000.f;
	GetCharacterMovement()->GroundFriction = 3.f;
	GetCharacterMovement()->MaxWalkSpeed = 600.f;
	GetCharacterMovement()->MaxFlySpeed = 600.f;

	// Note: The skeletal mesh and anim blueprint references on the Mesh component (inherited from Character) 
	// are set in the derived blueprint asset named MyCharacter (to avoid direct content references in C++)
}

//////////////////////////////////////////////////////////////////////////
// Input

void APlatformerCharacter::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent)
{
	// set up gameplay key bindings
	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &APlatformerCharacter::Jump);
	PlayerInputComponent->BindAction("Jump", IE_Released, this, &APlatformerCharacter::StopJumping);

	PlayerInputComponent->BindAction("PreviousWeapon", IE_Pressed, this, &APlatformerCharacter::PreviousWeapon);
	PlayerInputComponent->BindAction("NextWeapon", IE_Pressed, this, &APlatformerCharacter::NextWeapon);

	PlayerInputComponent->BindAction("ToggleFlashlight", IE_Pressed, this, &APlatformerCharacter::ToggleFlashlightClient);

	PlayerInputComponent->BindAction("Shift", IE_Pressed, this, &APlatformerCharacter::ShiftPressed);
	PlayerInputComponent->BindAction("Shift", IE_Released, this, &APlatformerCharacter::ShiftReleased);

	PlayerInputComponent->BindAxis("MoveRight", this, &APlatformerCharacter::MoveRight);
	PlayerInputComponent->BindAxis("LookUp", this, &APlatformerCharacter::LookUp);

	PlayerInputComponent->BindTouch(IE_Pressed, this, &APlatformerCharacter::TouchStarted);
	PlayerInputComponent->BindTouch(IE_Released, this, &APlatformerCharacter::TouchStopped);
}

void APlatformerCharacter::FireReq_Implementation(UAnimMontage* FireAnim, FRotator FireDirection, const TArray<TEnumAsByte<EObjectTypeQuery>>& Query, float Damage)
{
	bool bHasWeapon = false;
	if (!GetCurrentWeapon(bHasWeapon)) return;

	Fire(FireAnim, FireDirection, Query, Damage);

	GetWorldTimerManager().ClearTimer(FirePreDelay);
	TimerDelegate.BindLambda([&, FireAnim, FireDirection, Query, Damage]()
	{

		FVector StartTrace = GetCapsuleComponent()->GetComponentLocation();
		FVector EndTrace = GetCapsuleComponent()->GetComponentLocation() + (FireDirection.Vector() * 200.f);
		FHitResult HitRes;
		TArray<AActor*> Empty = { this };

		UKismetSystemLibrary::LineTraceSingleForObjects(
			GetWorld(),
			StartTrace,
			EndTrace,
			Query,
			false,
			Empty,
			EDrawDebugTrace::None,
			HitRes,
			true,
			FLinearColor::Red,
			FLinearColor::Green,
			0
		);

		IP_InterfaceHelper* HittenTarget = Cast<IP_InterfaceHelper>(HitRes.Actor);
		UP_FunctionLibrary::DealDamage(HittenTarget, CurrentWeapon, HitRes.ImpactPoint);
		
	});

	if (CurrentWeapon)
		GetWorldTimerManager().SetTimer(FirePreDelay, TimerDelegate, CurrentWeapon->PreFireDelay, false);

}

void APlatformerCharacter::Fire_Implementation(UAnimMontage* FireAnim, FRotator FireDirection, const TArray<TEnumAsByte<EObjectTypeQuery>>& Query, float Damage)
{
	PlayAnimMontage(FireAnim);
}

void APlatformerCharacter::SpawnWeaponInHands_Implementation(TSubclassOf<AP_WeaponBase> WeaponClass)
{
	if (CurrentWeapon)
		CurrentWeapon->Destroy();

	FActorSpawnParameters SpawnParams;
	SpawnParams.Owner = this;
	SpawnParams.Instigator = this;
	SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;

	FVector SpawnLoc(0, 0, 0);
	FRotator SpawnRot(0, 0, 0);

	CurrentWeapon = Cast<AP_WeaponBase>(GetWorld()->SpawnActor(WeaponClass, &SpawnLoc, &SpawnRot, SpawnParams));

	if (CurrentWeapon)
	{
		FAttachmentTransformRules TransformRules(EAttachmentRule::KeepRelative, EAttachmentRule::KeepRelative, EAttachmentRule::KeepRelative, false);

		CurrentWeapon->AttachToComponent(GetMesh(), TransformRules, CurrentWeapon->SocketToAttach);
		SetCurrentWeapon(CurrentWeapon);
	}
}

void APlatformerCharacter::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);
	HandleRotation();
}

void APlatformerCharacter::HandleRotation()
{
	if (!IsLocallyControlled()) return;

	if (bUpPressed)
		LookDirActual = LastLookDirUp;
	else
		LookDirActual = LastLookDirRight;

	switch (LookDirActual)
	{
	case ELookTo::Right:
		LookRotation = FRotator(0.f, -90, 0.f);
		break;
	case ELookTo::Left:
		LookRotation = FRotator(0.f, 90.f, 0.f);
		break;
	case ELookTo::Up:
		LookRotation = FRotator(90.f, 0.f, 0.f);
		break;
	case ELookTo::Down:
		LookRotation = FRotator(-90.f, 0.f, 0.f);
		break;
	case ELookTo::None:
		LookRotation = FRotator::ZeroRotator;
		break;
	}

	// Debug line
	FVector St = GetCapsuleComponent()->GetComponentLocation();
	FVector En = St + (LookRotation.Vector() * 300);

	//DrawDebugLine(GetWorld(), St, En, FColor::Purple, false);
}

void APlatformerCharacter::ActivateFlashlight_Implementation()
{
	if (Light)
	{
		Light->SetVisibility(true);
		FlashLightActive = true;
	}
}

void APlatformerCharacter::DeactivateFlashlight_Implementation()
{
	if (Light)
	{
		Light->SetVisibility(false);
		FlashLightActive = false;
	}
}

void APlatformerCharacter::ShiftPressed()
{
	bShiftPressed = true;
}

void APlatformerCharacter::ShiftReleased()
{
	bShiftPressed = false;
}

void APlatformerCharacter::ToggleFlashlightClient()
{

	if (InventoryComponent)
	{
		if (InventoryComponent->InventoryData.FlashLightCharge <= 0 && !FlashLightActive)
			return;
		else
			ToggleFlashlight();
	}

}

void APlatformerCharacter::ToggleFlashlight_Implementation()
{
	if (FlashLightActive)
	{
		DeactivateFlashlight();
	}
	else
	{
		if (InventoryComponent && InventoryComponent->InventoryData.FlashLightCharge > 0)
		{
			ActivateFlashlight();
			UE_LOG(LogTemp, Warning, TEXT("fll %f"), InventoryComponent->InventoryData.FlashLightCharge);
		}
	}
}

void APlatformerCharacter::SetInventoryComponent(UP_InventoryComponent* NewInventoryComponent)
{
	InventoryComponent = NewInventoryComponent;
}

UP_InventoryComponent* APlatformerCharacter::GetInventoryComponent(bool& bHasInventory)
{
	if (InventoryComponent)
	{
		bHasInventory = true;
		return InventoryComponent;
	}
	else
	{
		bHasInventory = false;
		return nullptr;
	}
}

void APlatformerCharacter::SetCurrentWeapon(AP_WeaponBase* NewWeapon)
{
	CurrentWeapon = NewWeapon;
}

AP_WeaponBase* APlatformerCharacter::GetCurrentWeapon(bool& bHasWeapon)
{
	if (CurrentWeapon)
	{
		bHasWeapon = true;
		return CurrentWeapon;
	}
	else
	{
		bHasWeapon = false;
		return nullptr;
	}
}

void APlatformerCharacter::TryFireCurrentWeapon(UAnimMontage* FireAnim, FRotator FireDirection, const TArray<TEnumAsByte<EObjectTypeQuery>>& Query, float Damage)
{
	bool bHasWeapon;
	AP_WeaponBase* CurWep = GetCurrentWeapon(bHasWeapon);

	if (!CurWep || !bHasWeapon) return;

	if (LastCooldownedWeapon != CurWep->GetClass())
	{
		LastCooldownedWeapon = CurWep->GetClass();
		SwitchCooldownIsFinished(false);
		
		GetWorld()->GetTimerManager().ClearTimer(WeaponCoolDownTimer);

		WeaponCDDelegate.BindLambda([&]()
		{
			SwitchCooldownIsFinished(true);
		});
		
		GetWorld()->GetTimerManager().SetTimer(WeaponCoolDownTimer, WeaponCDDelegate, CurWep->HitCooldown, false);
		FireReq(FireAnim, FireDirection, Query, Damage);
	}
	else
	{
		if (bWeaponCooldownFinished)
		{
			LastCooldownedWeapon = CurWep->GetClass();
			SwitchCooldownIsFinished(false);

			WeaponCDDelegate.BindLambda([&]()
			{
				SwitchCooldownIsFinished(true);
			});

			GetWorld()->GetTimerManager().SetTimer(WeaponCoolDownTimer, WeaponCDDelegate, CurWep->HitCooldown, false);

			FireReq(FireAnim, FireDirection, Query, Damage);
		}
	}
}

void APlatformerCharacter::SwitchCooldownIsFinished(bool bIsFinished)
{
	bWeaponCooldownFinished = bIsFinished;
}

void APlatformerCharacter::PreviousWeapon()
{
	if (InventoryComponent)
	{
		TSubclassOf<AP_WeaponBase> PrevWeapon = InventoryComponent->PreviousWeapon();
		if (PrevWeapon != AP_WeaponBase::StaticClass())
		{
			SpawnWeaponInHands(PrevWeapon);
		}
	}
}

void APlatformerCharacter::NextWeapon()
{
	if (InventoryComponent)
	{
		TSubclassOf<AP_WeaponBase> NextWeapon = InventoryComponent->NextWeapon();
		if (NextWeapon != AP_WeaponBase::StaticClass())
		{
			SpawnWeaponInHands(NextWeapon);
		}
	}
}

void APlatformerCharacter::MoveRight(float Value)
{
	// add movement in that direction
	AddMovementInput(FVector(0.f, -1.f, 0.f), Value);

	if (Value > 0)
	{
		LastLookDirRight = ELookTo::Right;
	} 
	else if (Value < 0)
	{
		LastLookDirRight = ELookTo::Left;
	}
}

void APlatformerCharacter::LookUp(float Val)
{

	if (Val != 0)
		bUpPressed = true;
	else
		bUpPressed = false;

	if (Val > 0)
	{
		LastLookDirUp = ELookTo::Up;
	}
	else if (Val < 0)
	{
		LastLookDirUp = ELookTo::Down;
	}
}

void APlatformerCharacter::Jump()
{
	ResetJumpState();
	UE_LOG(LogTemp, Warning, TEXT("CanJump %i"), CanJump());
	Super::Jump();
}

void APlatformerCharacter::StopJumping()
{
	Super::StopJumping();
}

void APlatformerCharacter::TouchStarted(const ETouchIndex::Type FingerIndex, const FVector Location)
{
	Jump();
}

void APlatformerCharacter::TouchStopped(const ETouchIndex::Type FingerIndex, const FVector Location)
{
	StopJumping();
}

void APlatformerCharacter::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(APlatformerCharacter, CurrentWeapon);
}

void APlatformerCharacter::AddDamage(float DamageVal)
{
	if (HealthComponent)
	{
		HealthComponent->AddDamage(DamageVal);
	}
}
