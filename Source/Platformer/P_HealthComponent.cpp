// Fill out your copyright notice in the Description page of Project Settings.


#include "P_HealthComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Components/CapsuleComponent.h"
#include "PlatformerCharacter.h"

// Sets default values for this component's properties
UP_HealthComponent::UP_HealthComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	SetIsReplicatedByDefault(true);
	SetIsReplicated(true);
	// ...
}


void UP_HealthComponent::SetHealthCompOwner(APlatformerCharacter* NewOwner)
{
	if (!NewOwner) return;
	CompOwner = NewOwner;
}

APlatformerCharacter* UP_HealthComponent::GetHealthCompOwner(bool& bHasOwner)
{
	if (CompOwner)
	{
		bHasOwner = true;
		return CompOwner;
	}
	else
	{
		bHasOwner = false;
		return nullptr;
	}
}

void UP_HealthComponent::AddDamage_Implementation(float AddingDamage)
{
	// Fix AI owning bug
	if (!CompOwner)
		CompOwner = Cast<APlatformerCharacter>(GetOwner());

	if (Involnurable || !CompOwner) return;

	AddingDamage /= Armor;

	if (HittenSound && AddingDamage > 0)
	{
		UGameplayStatics::PlaySoundAtLocation(GetWorld(), HittenSound, CompOwner->GetActorLocation());
	}

	if (AddingDamage >= Health)
	{
		Health = 0.f;
		SetLifeState(ELifeState::Dead);

		UCharacterMovementComponent* CharMovComp = Cast<UCharacterMovementComponent>(CompOwner->GetMovementComponent());
		if (CharMovComp)
		{
			CharMovComp->DisableMovement();
			CompOwner->GetCapsuleComponent()->SetCollisionEnabled(ECollisionEnabled::NoCollision);
			CompOwner->GetMesh()->SetCollisionEnabled(ECollisionEnabled::PhysicsOnly);
			CompOwner->GetMesh()->SetCollisionResponseToChannel(ECollisionChannel::ECC_Pawn, ECollisionResponse::ECR_Ignore);
			CompOwner->GetMesh()->SetSimulatePhysics(true);
		}

		OnDeath();

		Health = 0;
		return;
	}

	if (AddingDamage <= 0 && Health >= MaxHealth)
	{
		Health = MaxHealth;
		return;
	}

	Health -= AddingDamage;
}

void UP_HealthComponent::SetLifeState(ELifeState NewLifeState)
{
	LifeState = NewLifeState;
}

ELifeState UP_HealthComponent::GetLifeState()
{
	return LifeState;
}

// Called when the game starts
void UP_HealthComponent::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}


// Called every frame
void UP_HealthComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

void UP_HealthComponent::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(UP_HealthComponent, LifeState);
}

