// Fill out your copyright notice in the Description page of Project Settings.


#include "P_InventoryComponent.h"
#include "P_WeaponBase.h"
#include "PlatformerCharacter.h"
#include "P_PickableItem.h"
#include "Components/PointLightComponent.h"

// Sets default values for this component's properties
UP_InventoryComponent::UP_InventoryComponent()
{
	if (InventoryData.WeaponsInInventory.Num() > 0)
	{

	}
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;
	PrimaryComponentTick.TickInterval = 0.5;

	// ...
}


void UP_InventoryComponent::SetComponentOwner(AActor* NewOwner)
{
	if (NewOwner)
	{
		ComponentOwner = NewOwner;
	}
}

AActor* UP_InventoryComponent::GetComponentOwner(bool& bHasOwner)
{
	if (ComponentOwner)
	{
		bHasOwner = true;
		return ComponentOwner;
	}
	else
	{
		bHasOwner = false;
		return nullptr;
	}
}

void UP_InventoryComponent::FlashlightHandle(float DeltaSeconds)
{
	APlatformerCharacter* Char = Cast<APlatformerCharacter>(GetOwner());

	if (Char->IsLocallyControlled())
	{
		bool LightActive = Char->FlashLightActive;

		if (InventoryData.FlashLightCharge > 0 && LightActive)
		{
			InventoryData.FlashLightCharge -= DeltaSeconds;
		}
		else if (InventoryData.FlashLightCharge <= 0 && LightActive)
		{
			InventoryData.FlashLightCharge = 0;
			Char->ToggleFlashlight();
		}
	}

	OnBatteryChargeUpdated(InventoryData.FlashLightCharge);
}

void UP_InventoryComponent::SetCurrentWeapon(int32 WeaponIndex)
{
	// Maybe we dont need it?
}

bool UP_InventoryComponent::GetWeaponByIndex(int32 SearchIndex, TSubclassOf<AP_WeaponBase>& OuterWeaponClass)
{
	bool bResult = InventoryData.WeaponsInInventory.IsValidIndex(SearchIndex);
	
	if (bResult)
	{
		OuterWeaponClass = InventoryData.WeaponsInInventory[SearchIndex];

		CurrentSelectedWeapon.Index = SearchIndex;
		CurrentSelectedWeapon.WeaponClass = OuterWeaponClass;
	}
	
	return bResult;
}

TSubclassOf<AP_WeaponBase> UP_InventoryComponent::NextWeapon()
{
	if (InventoryData.WeaponsInInventory.Num() <= 0)
		return AP_WeaponBase::StaticClass();

	int32 NewIndex = CurrentSelectedWeapon.Index;
	TSubclassOf<AP_WeaponBase> SelectedWeapon;

	// Set correct index
	if (NewIndex == -1)
		NewIndex = 0;
	else
		NewIndex++;

	// Check is index in bounds
	bool bIsInBounds = InventoryData.WeaponsInInventory.IsValidIndex(NewIndex);

	// If out of bounds, set the first weapon
	if (!bIsInBounds)
		NewIndex = 0;

	// Assign var
	GetWeaponByIndex(NewIndex, SelectedWeapon);

	// Assign current struct data
	CurrentSelectedWeapon.SetCurrent(NewIndex, SelectedWeapon);

	return SelectedWeapon;
}

TSubclassOf<AP_WeaponBase> UP_InventoryComponent::PreviousWeapon()
{
	if (InventoryData.WeaponsInInventory.Num() <= 0)
		return AP_WeaponBase::StaticClass();

	int32 NewIndex = CurrentSelectedWeapon.Index;
	TSubclassOf<AP_WeaponBase> SelectedWeapon;

	// Set correct index
	if (NewIndex == -1)
		NewIndex = 0;
	else
		NewIndex--;

	// Check is index in bounds
	bool bIsInBounds = InventoryData.WeaponsInInventory.IsValidIndex(NewIndex);

	// If out of bounds, set the first weapon
	if (!bIsInBounds)
		NewIndex = InventoryData.WeaponsInInventory.Num() - 1;
	
	// Assign var
	GetWeaponByIndex(NewIndex, SelectedWeapon);

	// Assign current struct data
	CurrentSelectedWeapon.SetCurrent(NewIndex, SelectedWeapon);
	
	return SelectedWeapon;
}

TArray<TSubclassOf<AP_WeaponBase>> UP_InventoryComponent::GetWeapons()
{
	return InventoryData.WeaponsInInventory;
}

TArray<FP_ItemData> UP_InventoryComponent::GetItemsStruct()
{
	TArray<FP_ItemData> ItemData;
	ItemData = InventoryData.ItemsInInventory;

	return ItemData;
}

void UP_InventoryComponent::GetItemsStructRef(FP_ItemData& DataRef, int32 ItemIndex)
{
	DataRef = InventoryData.ItemsInInventory[ItemIndex];
}

void UP_InventoryComponent::SetItemsStruct(TArray<FP_ItemData> NewData)
{
	InventoryData.ItemsInInventory = NewData;
}

void UP_InventoryComponent::RemoveItemAtIndex(int32 ItemIndex)
{
	 TArray<FP_ItemData> ItemData = InventoryData.ItemsInInventory;
	 bool bIsIndexOk = ItemData.IsValidIndex(ItemIndex);

	 if (!bIsIndexOk) return;


	 FP_ItemData CurItem;
	 GetItemsStructRef(CurItem, ItemIndex);

	 if (CurItem.ItemCount > 1)
	 {
		 InventoryData.ItemsInInventory[ItemIndex].ItemCount--;
	 } 
	 else if (CurItem.ItemCount == 1)
	 {
		 CurItem.ItemCount = 0;
		 InventoryData.ItemsInInventory.RemoveAt(ItemIndex);
	 }

	 UE_LOG(LogTemp, Warning, TEXT("index %i, data %i"), ItemIndex, CurItem.ItemCount);
	
}

// Called when the game starts
void UP_InventoryComponent::BeginPlay()
{
	Super::BeginPlay();

	PrimaryComponentTick.TickInterval = 0.5;
	
	// ...
	
}


// Called every frame
void UP_InventoryComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	FlashlightHandle(DeltaTime);

	// ...
}

void UP_InventoryComponent::OnBatteryChargeUpdated_Implementation(float NewBatteryCharge)
{

}

void FSelectedWeaponInfo::SetCurrent(int32 NewIndex, TSubclassOf<AP_WeaponBase> NewWeapon)
{
	Index = NewIndex;
	WeaponClass = NewWeapon;
}
