// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "P_FunctionLibrary.h"
#include "Components/ActorComponent.h"
#include "P_InventoryComponent.generated.h"


class APlatformerCharacter;
class AP_WeaponBase;

USTRUCT()
struct FSelectedWeaponInfo
{
	GENERATED_BODY()

	int32 Index = -1;
	TSubclassOf<AP_WeaponBase> WeaponClass;

	void SetCurrent(int32 NewIndex, TSubclassOf<AP_WeaponBase> NewWeapon);

};

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent), Blueprintable )
class PLATFORMER_API UP_InventoryComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UP_InventoryComponent();

	// Vars
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	AActor* ComponentOwner = nullptr;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	FP_Inventory InventoryData;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	int32 ItemSlotMaximumItems = 10;

	FSelectedWeaponInfo CurrentSelectedWeapon;

	// Funcs
	UFUNCTION(BlueprintCallable)
	void SetComponentOwner(AActor* NewOwner);

	UFUNCTION(BlueprintCallable)
	AActor* GetComponentOwner(bool& bHasOwner);

	UFUNCTION()
	void FlashlightHandle(float DeltaSeconds);

	// Maybe we dont need it?
	UFUNCTION(BlueprintCallable)
	void SetCurrentWeapon(int32 WeaponIndex);

	bool GetWeaponByIndex(int32 SearchIndex, TSubclassOf<AP_WeaponBase>& OuterWeaponClass);

	UFUNCTION(BlueprintCallable)
	TSubclassOf<AP_WeaponBase> NextWeapon();
	UFUNCTION(BlueprintCallable)
	TSubclassOf<AP_WeaponBase> PreviousWeapon();

	UFUNCTION(BlueprintCallable)
	TArray<TSubclassOf<AP_WeaponBase>> GetWeapons();

	UFUNCTION(BlueprintCallable)
	TArray<FP_ItemData> GetItemsStruct();

	UFUNCTION(BlueprintCallable)
	void GetItemsStructRef(FP_ItemData& DataRef, int32 ItemIndex);

	UFUNCTION(BlueprintCallable)
	void SetItemsStruct(TArray<FP_ItemData> NewData);

	UFUNCTION(BlueprintCallable)
	void RemoveItemAtIndex(int32 ItemIndex);


protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	UFUNCTION(BlueprintNativeEvent)
	void OnBatteryChargeUpdated(float NewBatteryCharge);

		
};
