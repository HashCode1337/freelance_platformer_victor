// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "P_CMComponent.generated.h"

/**
 * 
 */
UCLASS()
class PLATFORMER_API UP_CMComponent : public UCharacterMovementComponent
{
	GENERATED_BODY()

public:

	virtual bool CanAttemptJump() const override;
	virtual bool IsMovingOnGround() const override;
	
};
