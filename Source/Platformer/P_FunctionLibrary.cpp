// Fill out your copyright notice in the Description page of Project Settings.


#include "P_FunctionLibrary.h"
#include "P_InterfaceHelper.h"
#include "P_WeaponBase.h"
#include "P_PickableItem.h"
#include "PlatformerGameMode.h"
#include "Kismet/GameplayStatics.h"
#include "PlatformerCharacter.h"
#include "Engine/World.h"

void UP_FunctionLibrary::DealDamage(IP_InterfaceHelper* Target, AP_WeaponBase* Weapon, FVector Location)
{
	if (!Target || !Weapon) return;

	ETargetType TT = Target->GetTargetType();
	switch (TT)
	{
	case ETargetType::Block:
	{
		Target->AddDamage(Weapon->EnvironmentDamage);
		Weapon->PlayEnvirHitSound();
		break;
	}
	case ETargetType::Character:
	{
		Target->AddDamage(Weapon->FightDamage);
		Weapon->PlayHitSound();
		break;
	}
	default:
		break;
	}
}

bool UP_FunctionLibrary::UseItem(APawn* ItemOwner, TSubclassOf<AP_PickableItem> ItemClass)
{
	bool Result = false;
	if (ItemOwner && ItemOwner->IsLocallyControlled())
	{

		AP_PickableItem* DefaultObject = Cast<AP_PickableItem>(ItemClass->GetDefaultObject());
		EItemType ItemType = DefaultObject->ItemType;

		if (ItemType == EItemType::Coal)
		{

		}
		else if (ItemType == EItemType::Diamond)
		{

		}
		else if (ItemType == EItemType::Medkit)
		{
			APlatformerCharacter* Char = Cast<APlatformerCharacter>(ItemOwner);
			if (Char)
			{
				Char->AddDamage(-20.f);
				Result = true;
			}
		}
	}
	return Result;
}

bool UP_FunctionLibrary::DropItem(APawn* ItemOwner, TSubclassOf<AP_PickableItem> ItemClass)
{
	bool bIsCanPlace = true;

	if (!ItemOwner->IsLocallyControlled()) return false;

	FVector EndPos = ItemOwner->GetActorLocation();//ItemOwner->GetActorLocation() + (ItemOwner->GetActorRotation().Vector() * 200);

	APlatformerGameMode* GM = Cast<APlatformerGameMode>(UGameplayStatics::GetGameMode(ItemOwner->GetWorld()));
	APlatformerCharacter* PCh = Cast<APlatformerCharacter>(ItemOwner);
	if (GM && PCh)
	{
		TArray<AActor*> OverlappingActors;
		UBoxComponent* ColBox = PCh->DropItemHolder;

		ColBox->GetOverlappingActors(OverlappingActors, AActor::StaticClass());
		UE_LOG(LogTemp, Warning, TEXT("OL Actors num - %i"), OverlappingActors.Num());

		if (OverlappingActors.Num() > 0) return false;

		GM->EasySpawnAtLocation(ItemClass, PCh->DropItemHolder->GetComponentLocation());
	}

	return bIsCanPlace;

}
