// Fill out your copyright notice in the Description page of Project Settings.


#include "P_PickableItem.h"

// Sets default values
AP_PickableItem::AP_PickableItem()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	RootComp = CreateDefaultSubobject<USceneComponent>(TEXT("Root"));
	RootComponent = RootComp;

	BoxCollision = CreateDefaultSubobject<UBoxComponent>(TEXT("PickupRange"));
	BoxCollision->SetupAttachment(RootComp);
	BoxCollision->SetBoundsScale(1);
	BoxCollision->SetCollisionProfileName(FName(TEXT("Pickup")));
	BoxCollision->OnComponentBeginOverlap.AddDynamic(this, &AP_PickableItem::OnHitboxBeginOverlapp);

	PickUpMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("PickUpMesh"));
	PickUpMesh->SetupAttachment(RootComp);
	PickUpMesh->SetCollisionEnabled(ECollisionEnabled::NoCollision);

}

// Called when the game starts or when spawned
void AP_PickableItem::BeginPlay()
{
	Super::BeginPlay();


}

// Called every frame
void AP_PickableItem::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AP_PickableItem::OnHitboxBeginOverlapp_Implementation(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	GEngine->AddOnScreenDebugMessage(3, 5, FColor::Blue, "OVERLAPP");
}


