// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "P_FunctionLibrary.generated.h"

class IP_InterfaceHelper;
class AP_WeaponBase;
class AP_PickableItem;

UENUM()
enum class EItemType : uint8
{
	None UMETA(DisplayName = "None"),
	Coal UMETA(DisplayName = "Coal"),
	Battery UMETA(DisplayName = "Battery"),
	Diamond UMETA(DisplayName = "Diamond"),
	Medkit UMETA(DisplayName = "Medkit")
};

UENUM()
enum class ETargetType : uint8
{
	Unknown UMETA(DisplayName = "Unknown"),
	Character UMETA(DisplayName = "Character"),
	Enemy UMETA(DisplayName = "Enemy"),
	Block UMETA(DisplayName = "Block")
};

UENUM()
enum class ELookTo : uint8
{
	None UMETA(DisplayName = "None"),
	Left UMETA(DisplayName = "Left"),
	Right UMETA(DisplayName = "Right"),
	Up UMETA(DisplayName = "Up"),
	Down UMETA(DisplayName = "Down")
};

USTRUCT(BlueprintType)
struct FP_WeaponInfo
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FName WeaponName = TEXT("WeaponBase");
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FName SocketToAttach = TEXT("WeaponSocket");
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UStaticMeshComponent* WeaponMesh = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float EnvironmentDamage = 0.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float FightDamage = 0.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float HitCooldown = 0.7f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float PreFireDelay = 0.7f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	USoundBase* HitEnvironmentSound = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	USoundBase* HitEnemySound = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UAnimMontage* HitAnimation = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	AActor* WeaponOwner = nullptr;
};

USTRUCT(BlueprintType)
struct FP_ItemData
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ItemData")
	TSubclassOf<AP_PickableItem> Item;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ItemData")
	int32 ItemCount;
};

USTRUCT(BlueprintType)
struct FP_Inventory
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Inventory")
	TArray<TSubclassOf<AP_WeaponBase>> WeaponsInInventory;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Inventory")
	TArray<FP_ItemData> ItemsInInventory;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Inventory")
	bool bHasFlashLight = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Inventory")
	float FlashLightCharge = 60.f;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Inventory")
	bool bFlashLightPowerOn = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Inventory")
	UCurveFloat* LightCurve = nullptr;
};

UCLASS()
class PLATFORMER_API UP_FunctionLibrary : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()

public:

	static void DealDamage(IP_InterfaceHelper* Target, AP_WeaponBase* Weapon, FVector Location);

	UFUNCTION(BlueprintCallable)
	static bool UseItem(APawn* ItemOwner, TSubclassOf<AP_PickableItem> ItemClass);

	UFUNCTION(BlueprintCallable)
	static bool DropItem(APawn* ItemOwner, TSubclassOf<AP_PickableItem> ItemClass);
	
};
