// Fill out your copyright notice in the Description page of Project Settings.


#include "P_WeaponBase.h"
#include "Kismet/GameplayStatics.h"

// Sets default values
AP_WeaponBase::AP_WeaponBase()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;


	RootComp = CreateDefaultSubobject<USceneComponent>(TEXT("RootHolder"));
	RootComponent = RootComp;

	WeaponMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("WeaponMesh"));
	WeaponMesh->SetupAttachment(RootComp);
	WeaponMesh->SetCastShadow(false);

	CurrentWeaponInfo.EnvironmentDamage = EnvironmentDamage;
	CurrentWeaponInfo.FightDamage = FightDamage;
	CurrentWeaponInfo.HitAnimation = HitAnimation;
	CurrentWeaponInfo.HitCooldown = HitCooldown;
	CurrentWeaponInfo.HitEnemySound = HitEnemySound;
	CurrentWeaponInfo.HitEnvironmentSound = HitEnvironmentSound;
	CurrentWeaponInfo.PreFireDelay = PreFireDelay;
	CurrentWeaponInfo.SocketToAttach = SocketToAttach;
	CurrentWeaponInfo.WeaponMesh = WeaponMesh;
	CurrentWeaponInfo.WeaponName = WeaponName;
	CurrentWeaponInfo.WeaponOwner = WeaponOwner;

	SetReplicates(true);
}

void AP_WeaponBase::SetWeaponOwner(AActor* NewOwner)
{
	WeaponOwner = NewOwner;
}

AActor* AP_WeaponBase::GetWeaponOwner()
{
	return WeaponOwner;
}

void AP_WeaponBase::GetWeaponInfo(FP_WeaponInfo& OutWeaponInfo)
{
	OutWeaponInfo = CurrentWeaponInfo;
}

bool AP_WeaponBase::SpawnWeaponOnActorHands(AActor* NewOwner, FName SocketName /*= "WeaponSocket"*/)
{
	return false;
}

bool AP_WeaponBase::RemoveFromActor()
{
	return false;
}

void AP_WeaponBase::PlayHitSound_Implementation()
{
	if (HitEnemySound)
	{
		UGameplayStatics::PlaySoundAtLocation(GetWorld(), HitEnemySound, GetActorLocation(), 1);
	}
}

void AP_WeaponBase::PlayEnvirHitSound_Implementation()
{
	if (HitEnvironmentSound)
	{
		UGameplayStatics::PlaySoundAtLocation(GetWorld(), HitEnvironmentSound, GetActorLocation(), 1);
	}
}

// Called when the game starts or when spawned
void AP_WeaponBase::BeginPlay()
{
	Super::BeginPlay();

	if (HasAuthority())
		SetReplicates(true);

}

// Called every frame
void AP_WeaponBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	

}