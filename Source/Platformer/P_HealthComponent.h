// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Net/UnrealNetwork.h"
#include "P_HealthComponent.generated.h"

class APlatformerCharacter;

UENUM(BlueprintType)
enum class ELifeState : uint8
{
	Alive UMETA(DisplayName = "Alive"),
	Dead UMETA(DisplayName = "Dead")
};

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent), Blueprintable )
class PLATFORMER_API UP_HealthComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UP_HealthComponent();
	
public:

	// Vars
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Health Settings")
	float Health = 100.f;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Health Settings")
	float MaxHealth = 100.f;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Health Settings")
	float Armor = 1.f;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Health Settings")
	bool Involnurable = false;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Health Settings")
	USoundBase* HittenSound= nullptr;
	UPROPERTY(Replicated, VisibleDefaultsOnly ,BlueprintReadOnly, Category = "Health Settings")
	ELifeState LifeState = ELifeState::Alive;

	// Funcs
	UFUNCTION(BlueprintCallable)
	void SetHealthCompOwner(APlatformerCharacter* NewOwner);
	UFUNCTION(BlueprintCallable)
	APlatformerCharacter* GetHealthCompOwner(bool& bHasOwner);

	UFUNCTION(NetMulticast, Reliable, BlueprintCallable)
	void AddDamage(float AddingDamage/*, float& DamageDealed*/);
	UFUNCTION(BlueprintCallable)
	void SetLifeState(ELifeState NewLifeState = ELifeState::Alive);
	UFUNCTION(BlueprintCallable)
	ELifeState GetLifeState();

	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
	void OnDeath();
	
	

private:

	APlatformerCharacter* CompOwner = nullptr;


protected:
	// Called when the game starts
	virtual void BeginPlay() override;


public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
	void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

		
};
