// Fill out your copyright notice in the Description page of Project Settings.


#include "P_CMComponent.h"
#include "PlatformerCharacter.h"
#include "Components/CapsuleComponent.h"
#include "DrawDebugHelpers.h"

bool UP_CMComponent::CanAttemptJump() const
{
	bool bCanClimb = false;

	APlatformerCharacter* CompOwner = Cast<APlatformerCharacter>(GetOwner());
	UCapsuleComponent* CapsuleComp = CompOwner->GetCapsuleComponent();
	
	if (CompOwner)
	{
		

		FHitResult HitResult;

		FVector StartLine = CapsuleComp->GetComponentLocation();
		FVector EndLine = StartLine + (CompOwner->LookRotation.Vector() * 30);

		FCollisionQueryParams QParams;

		GetWorld()->LineTraceSingleByProfile(HitResult, StartLine, EndLine, TEXT("Climb"), QParams);

		DrawDebugSphere(GetWorld(), HitResult.ImpactPoint, 32.f, 12, FColor::Red, false, 10.f);
		DrawDebugLine(GetWorld(), StartLine, EndLine, FColor::Red, false, 5);

		bCanClimb = HitResult.bBlockingHit;
		
		if (bCanClimb && !IsMovingOnGround())
		{
			const FVector Vec = FVector(0, /*HitResult.ImpactNormal.Y * 500*/ 0 ,1000);
			CompOwner->GetCharacterMovement()->Launch(Vec);
			//CompOwner->MoveRight(HitResult.ImpactNormal.Y * -1);
			//UE_LOG(LogTemp, Warning, TEXT("V %s"), *EndLine.ToString());
		}
	}

	return (IsJumpAllowed() || bCanClimb) &&
		!bWantsToCrouch &&
		(IsMovingOnGround() || IsFalling()); // Falling included for double-jump and non-zero jump hold time, but validated by character.
}

bool UP_CMComponent::IsMovingOnGround() const
{
	//return true;
	return ((MovementMode == MOVE_Walking) || (MovementMode == MOVE_NavWalking)) && UpdatedComponent;
}