// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "P_FunctionLibrary.h"
#include "P_InterfaceHelper.h"
#include "P_WeaponBase.generated.h"

UCLASS()
class PLATFORMER_API AP_WeaponBase : public AActor/*, public IP_InterfaceHelper*/
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AP_WeaponBase();

	// Vars
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "WeaponSettings")
	FName WeaponName = TEXT("WeaponBase");

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "WeaponSettings")
	FName SocketToAttach = TEXT("weapon");

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "WeaponSettings")
	UStaticMeshComponent* WeaponMesh = nullptr;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "WeaponSettings")
	float EnvironmentDamage = 0.f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "WeaponSettings")
	float FightDamage = 0.f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "WeaponSettings")
	float HitCooldown = 0.7f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "WeaponSettings")
	float PreFireDelay = 0.7f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "WeaponSettings")
	USoundBase* HitEnvironmentSound = nullptr;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "WeaponSettings")
	USoundBase* HitEnemySound = nullptr;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "WeaponSettings")
	UAnimMontage* HitAnimation = nullptr;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "WeaponSettings")
	UTexture2D* WeaponIcon = nullptr;

	FP_WeaponInfo CurrentWeaponInfo;

	// Funcs
	UFUNCTION(BlueprintCallable)
	void SetWeaponOwner(AActor* NewOwner);
	
	UFUNCTION(BlueprintCallable)
	AActor* GetWeaponOwner();

	void GetWeaponInfo(FP_WeaponInfo& OutWeaponInfo);

	UFUNCTION(BlueprintCallable)
	bool SpawnWeaponOnActorHands(AActor* NewOwner, FName SocketName = "WeaponSocket");

	UFUNCTION(BlueprintCallable)
	bool RemoveFromActor();



private:

	AActor* WeaponOwner = nullptr;
	USceneComponent* RootComp;




	// Funcs

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Interface
	UFUNCTION(NetMulticast, Reliable, BlueprintCallable)
	void PlayHitSound();
	UFUNCTION(NetMulticast, Reliable, BlueprintCallable)
	void PlayEnvirHitSound();

};
