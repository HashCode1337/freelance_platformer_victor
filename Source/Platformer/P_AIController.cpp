// Fill out your copyright notice in the Description page of Project Settings.


#include "P_AIController.h"
#include "BrainComponent.h"
#include "BehaviorTree/BlackboardComponent.h"

void AP_AIController::BeginPlay()
{
	Super::BeginPlay();

	if (BT)
	{
		bool res = RunBehaviorTree(BT);
		UE_LOG(LogTemp, Warning, TEXT("IsRan %i"), res);
		GetBlackboardComponent()->SetValueAsVector("FirstLoc", FVector(0.f, 570.f, 500.f));
		GetBlackboardComponent()->SetValueAsVector("SecondLoc", FVector(0.f, 260.f, 500.f));
	}


}
